'use strict';

angular.module('confusionApp').controller('MenuController', ['$scope', 'menuFactory', function($scope, menuFactory) {

            $scope.showMenu = false;
            $scope.message = "Loading ...";
            $scope.dishes = menuFactory.getDishes().query(
                function(response) {
                    $scope.dishes = response;
                    $scope.showMenu = true;
                },
                function(response) {
                    $scope.message = "Error: " + response.status + " " + response.statusText;
                }
            );

            $scope.tab = 1;

            $scope.filtText = '';

            $scope.select = function(setTab) {
                this.tab = setTab;

                if (setTab === 2) {
                    this.filtText = "appetizer";
                }
                else if (setTab === 3) {
                    this.filtText = "mains";
                }
                else if (setTab === 4) {
                    this.filtText = "dessert";
                }
                else {
                    this.filtText = "";
                }
            };

            $scope.isSelected = function(checkTab) {
                return (this.tab === checkTab);
            };

            $scope.showDetails = false;

            $scope.toggleDetails = function() {
                $scope.showDetails = !$scope.showDetails;
            };

}])

.controller('ContactController', ['$scope', function($scope) {

    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };

    $scope.channels = [{value:"tel", label:"Tel."}, {value:"email", label:"E-mail"}];

    $scope.invalidChannelSelection = false;

}])

.controller('FeedbackController', ['$scope', 'feedbackFactory', function($scope, feedbackFactory) {

    $scope.sendFeedback = function() {
        console.log($scope.feedback);
        if ($scope.feedback.agree && $scope.feedback.mychannel === "") {
            $scope.invalidChannelSelection = true;
        }
        else {
            feedbackFactory.getFeedback().save($scope.feedback);

            $scope.invalidChannelSelection = false;
            $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
            $scope.feedback.mychannel = "";
            $scope.feedbackForm.$setPristine();
            console.log($scope.feedback);
        }
    };

}])

.controller('DishDetailController', ['$scope', '$stateParams', 'menuFactory', function($scope, $stateParams, menuFactory) {

    $scope.showDish = false;
    $scope.message = "Loading ...";
	$scope.dish = menuFactory.getDishes().get({id: parseInt($stateParams.id, 10)}).$promise.then(
        function(response) {
            $scope.dish = response;
            $scope.showDish = true;
        },
        function(response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );

}])

.controller('DishCommentController', ['$scope', 'menuFactory', function($scope, menuFactory) {

    $scope.comment = {author:"", rating:5, comment:"" };

    $scope.submitComment = function() {
        var comment = $scope.comment;
        // Convert rating from string to integer
        comment.rating = parseInt(comment.rating, 10);
        // Get date as ISO string
        comment.date = new Date().toISOString();
        // Add comment to list of comments
        $scope.dish.comments.push(comment);
        // Save comments
        menuFactory.getDishes().update({id: $scope.dish.id}, $scope.dish);
        // Reset form
        $scope.comment = {author:"", rating:5, comment:"" };
        $scope.commentForm.$setPristine();
    };

}])

.controller('IndexController', ['$scope', 'menuFactory', 'corporateFactory', function($scope, menuFactory, corporateFactory) {

    $scope.showDish = false;
    $scope.dishMessage = "Loading ...";
	$scope.dish = menuFactory.getDishes().get({id: 0}).$promise.then(
        function(response) {
            $scope.dish = response;
            $scope.showDish = true;
        },
        function(response) {
            $scope.dishMessage = "Error: " + response.status + " " + response.statusText;
        }
    );

    $scope.showPromotion = false;
    $scope.promotionMessage = "Loading ...";
    $scope.promotion = menuFactory.getPromotions().get({id: 0}).$promise.then(
        function(response) {
            $scope.promotion = response;
            $scope.showPromotion = true;
        },
        function(response) {
            $scope.promotionMessage = "Error: " + response.status + " " + response.statusText;
        }
    );

    $scope.showLeader = false;
    $scope.leaderMessage = "Loading ...";
    $scope.leader = corporateFactory.getLeaders().get({id: 3}).$promise.then(
        function(response) {
            $scope.leader = response;
            $scope.showLeader = true;
        },
        function(response) {
            $scope.leaderMessage = "Error: " + response.status + " " + response.statusText;
        }
    );

}])

.controller('AboutController', ['$scope', 'corporateFactory', function($scope, corporateFactory) {

    $scope.showLeaders = false;
    $scope.message = "Loading ...";
    $scope.leaders = corporateFactory.getLeaders().query(
        function(response) {
            $scope.leaders = response;
            $scope.showLeaders = true;
        },
        function(response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );

}])

;
